#include <iostream>
#include <vector>
#include <math.h>
#include <time.h>
using namespace std;

class layer;

float sigmoid(float x, bool deriv) {
	if (deriv)
		return x * (1 - x);
	return 1 / (1 + exp(-x));
}

float linear(float x, bool deriv) {
	return x;
}

float binary(float x, bool deriv) {
	if (deriv)
		return 0;
	return x > 0.5f ? 1 : 0;
}

class neuron {
	float (*activate_func)(float x, bool deriv);
	layer* parent;
	int number;

public:
	float out;
	float sigma;

	neuron(float (*activate_func)(float, bool), layer* parent, int number);
	void activate(float inp);
	void activate();
	void calc_sigma(float err);
};

class layer {
public:
	layer* prev;
	layer* next;
	vector<neuron*> neurons;
	vector<float>* inp_weights;
	vector<float>* out_weights;

	layer(int size, float (*activate)(float, bool), layer* prev);
	layer* activate(vector<float> inp_values);
	float train(vector<float> x, vector<float> y, float learning_rate);
};

neuron::neuron(float (*activate_func)(float, bool), layer* parent, int number) {
	this->activate_func = activate_func;
	this->parent = parent;
	this->number = number;
}

void neuron::activate(float inp) {
	out = (*(this->activate_func))(inp, false);
}

void neuron::activate() {
	float inp = 0;
	for (int i = 0; i < parent->prev->neurons.size(); i++)
	{
		inp += parent->inp_weights[i][number] * parent->prev->neurons[i]->out;
	}
	activate(inp);
}

void neuron::calc_sigma(float err = -100) {
	if (err > -90) {
		sigma = -err * (*activate_func)(out, true);
	}
	else {
		sigma = 0;
		for (int i = 0; i < parent->next->neurons.size(); i++) {
			sigma += parent->out_weights[number][i] * parent->next->neurons[i]->sigma;
		}
		sigma *= (*activate_func)(out, true);
	}
}

layer::layer(int size, float (*activate)(float, bool), layer* prev) {
	this->prev = prev;
	for (int i = 0; i < size; i++)
	{
		neurons.push_back(new neuron(activate, this, i));
	}

	if (prev) {
		prev->next = this;
		prev->out_weights = new vector<float>[prev->neurons.size()];
		for (int i = 0; i < prev->neurons.size(); i++)
		{
			for (int j = 0; j < size; j++)
			{
				float s = rand() * 1. / RAND_MAX;
				prev->out_weights[i].push_back(s);
			}
		}
		inp_weights = prev->out_weights;
	}
}

layer* layer::activate(vector<float> inp_values = {}) {
	for (int i = 0; i < neurons.size(); i++) {
		if (inp_values.size() > 0)
			neurons[i]->activate(inp_values[i]);
		else
			neurons[i]->activate();
	}
	if (next) {
		return next->activate();
	}
	else {
		return this;
	}
}

float layer::train(vector<float> x, vector<float> y, float learning_rate = 0.0001) {
	if (!next) {
		layer* out = activate(x);
		vector<float> error;
		for (int i = 0; i < y.size(); i++) {
			error.push_back(y[i] - out->neurons[i]->out);
		}
		for (int i = 0; i < error.size(); i++) {
			neurons[i]->calc_sigma(error[i]);
		}
		float err = 0;
		for (auto x : error) {
			err += x * x;
		}
		return err;
	}
	float err = next->train(x, y, learning_rate);
	for (int i = 0; i < neurons.size(); i++) {
		neurons[i]->calc_sigma();
	}

	// update weights
	for (int i = 0; i < neurons.size(); i++) {
		for (int j = 0; j < next->neurons.size(); j++) {
			out_weights[i][j] += next->neurons[j]->sigma * neurons[i]->out * learning_rate;
		}
	}

	return err;
}

int main() {
	srand(time(0));
	layer inp = layer(2, linear, 0);
	layer hidden1 = layer(2, sigmoid, &inp);
	layer out = layer(1, sigmoid, &hidden1);
	vector<vector<float>> x = {
		{ 0, 0 },
		{ 0, 1 },
		{ 1, 0 },
		{ 1, 1 }
	};
	vector<vector<float>> y = {
		{ 0 },
		{ 0 },
		{ 1 }, 
		{ 1 }
	};
	for (int i = 0; i < 4; i++) {
		layer* y = inp.activate(x[i]);
		for (auto x : y->neurons) {
			cout << x->out << " ";
		}
		cout << endl;
	}
	while (true) {
		cout << inp.train(x[2], y[2]) << endl;
	}
}